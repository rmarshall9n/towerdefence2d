﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Audio;

namespace MyTowerDefence
{
    public class Tower : GameModel
    {
        public static float TILE_DIAMETER = 0.6f;
        public static float TILE_RADIUS = 0.3f;

        private Vector3 BOUNDS
        {
            get { return new Vector3(-TILE_RADIUS, -TILE_RADIUS, TILE_RADIUS); }
        }

        protected int level;
        protected int cost;
        protected int damage;
        protected float radius;
        protected float speed;
        protected float upgradeTimer = 0, upgradeDuration;
        private float bulletTimer;

        protected Enemy target;
        private List<Bullet> bulletList = new List<Bullet>();

        private Model bulletModel;

        private Texture2D bulletTexture;
        private Texture2D starTexture;
                
        public int Cost
        {
            get { return cost; }
        }
        public Enemy Target
        {
            get { return target; }
        }

        public Tower(Vector3 position, Vector3 rotation, float scale, Model model, Model bulletModel)
            : base(position, rotation, scale, model)
        {
            this.bulletModel = bulletModel;
            boundingBox.Min = position + BOUNDS;
            boundingBox.Max = position - BOUNDS;

            bulletTimer = 0;
            upgradeDuration = 2000;

            level = 0;
            LevelUp(); // initializes the towers stats
            
        }

        public override void Update(GameTime gameTime)
        {
            /*
            // applies upgrade effect to tower
            if (upgradeTimer > 0)
            {
                upgradeTimer -= (float)gameTime.ElapsedGameTime.TotalMilliseconds;
                return;
            }
            */
            
            // calculate if enough time is passed to shoot another bullet
            bulletTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;
            
            if (bulletTimer > speed && target != null)
            {
                Game.soundManager.playSound("fire");
                bulletList.Add(new Bullet(position, Vector3.Zero, 0.2f, bulletModel));
                bulletTimer = 0;
            }

            // rotate the tower towards target
            FaceTarget();
            
            // cycle through each bullet to control its behaviour
            for (int i = 0; i < bulletList.Count; i++)
            {
                Bullet b = bulletList[i];


                if (!IsInRange(b.Position))
                    bulletList.Remove(b);

                if (target != null)
                {
                    b.SetVelocity(target.Position);
                }
                b.Update(gameTime);

                // if bullet collides with enemy
                if (target != null && Vector3.Distance(b.Position, target.Position) <= 0.2f)
                {
                    target.Health -= damage;
                    ApplyEffect();
                    bulletList.Remove(b);
                    break;
                }
            }

            base.Update(gameTime);
        }

        public override void Draw(Camera camera)
        {
            
             foreach (Bullet b in bulletList)
                b.Draw(camera);
            /*
            // draw stars
            if (level >= 2)
                spriteBatch.Draw(starTexture, position, Color.White);

            if (level >= 3)
                spriteBatch.Draw(starTexture, Vector2.Add(position, new Vector2(8, 0)), Color.White);

            // if were upgrading draw the tower black
            if (upgradeTimer > 0)
            {
                base.Draw(spriteBatch, Color.Black);
            }
            else
            {
                base.Draw(spriteBatch, Color.White);
            }
            */

            base.Draw(camera);
        }

        private bool IsInRange(Vector3 targetPos)
        {
            // check if the target pos is in the towers radius
            return Vector3.Distance(position, targetPos) <= radius;
        }

        public void FindClosestEnemy(List<Enemy> enemies)
        {
            // cycle through enemies and set the closest as the target.
            Enemy closest = null;
            float closestDistance = radius;

            foreach (Enemy e in enemies)
            {
                float distance = Vector3.Distance(e.Position, position);

                if (distance < closestDistance)
                {
                    distance = closestDistance;
                    closest = e;
                }
            }

            target = closest;
        }

        public void FaceTarget()
        {
            if (target != null)
            {
                // face the direction of the target
                Vector3 direction = position - target.Position;
                direction.Normalize();

                Rotation = new Vector3(0,(float)Math.Atan2(-direction.X, direction.Z),0);
            }
        }

        protected virtual void ApplyEffect() { }

        public virtual void LevelUp()
        {
            if (upgradeTimer <= 0)
            {
                upgradeTimer = upgradeDuration;
                level++;
            }
        }
    }
}
