﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MyTowerDefence
{
    class PoisonTower : Tower
    {
        int poisonTotalDamage;
        float poisonDuration;

        public PoisonTower(Vector3 position, Vector3 rotation, float scale, Model model, Model bulletModel)
            : base(position, rotation, scale, model, bulletModel)
        {
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(Camera camera)
        {
            base.Draw(camera);
        }

        protected override void ApplyEffect()
        {
            target.Poisoned(poisonTotalDamage, poisonDuration);   
        }

        public override void LevelUp()
        {
            base.LevelUp();

            switch (level)
            {
                case 1:
                    radius = 100;
                    cost = 100;
                    damage = 10;
                    speed = 500;
                    poisonTotalDamage = 10;
                    poisonDuration = 5;
                    break;

                case 2:
                    radius = 200;
                    cost = 100;
                    damage = 10;
                    speed = 500;
                    break;

                case 3:
                    radius = 200;
                    cost = 100;
                    damage = 10;
                    speed = 500;
                    break;
            }
        }
    }
}
