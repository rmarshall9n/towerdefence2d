﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MyTowerDefence
{
    class SlowTower : Tower
    {
        int slowPercent;
        float slowDuration;

        public SlowTower(Vector3 position, Vector3 rotation, float scale, Model model, Model bulletModel)
            : base(position, rotation, scale, model, bulletModel)
        {
        }

        public override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        public override void Draw(Camera camera)
        {
            base.Draw(camera);
        }

        protected override void ApplyEffect()
        {
            target.Slowed(slowPercent, slowDuration);
        }

        public override void LevelUp()
        {
            base.LevelUp();

            switch (level)
            {
                case 1:
                    radius = 100;
                    cost = 100;
                    damage = 10;
                    speed = 500;
                    slowDuration = 1;
                    slowPercent = 30;
                    break;

                case 2:
                    radius = 200;
                    cost = 100;
                    damage = 10;
                    speed = 500;
                    break;

                case 3:
                    radius = 200;
                    cost = 100;
                    damage = 10;
                    speed = 500;
                    break;
            }
        }
    }
}
