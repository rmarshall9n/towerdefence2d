﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Media;
namespace MyTowerDefence
{
    public class mySound
    {
        private SoundEffect sound;
        private string name;

        public SoundEffect Sound
        {
            get { return sound; }
        }       

        public string Name
        {
            get { return name; }
        }

        public mySound(SoundEffect se, string n)
        {
            sound = se;
            name = n;
        }
    };

    public class Sound
    {
        private List<mySound> soundList = new List<mySound>();

        public Sound()
        {
        }

        public void LoadContent(ContentManager content)
        {
            // load all sounds here
            soundList.Add(new mySound(content.Load<SoundEffect>("Sounds/fire"), "fire"));
            soundList.Add(new mySound(content.Load<SoundEffect>("Sounds/beep"), "beep"));
        }

        public void playSound(string name)
        {
            foreach (mySound s in soundList)
            {
                if (s.Name == name)
                {
                    s.Sound.Play() ;
                    break;
                }
            }
        }

    }
}
