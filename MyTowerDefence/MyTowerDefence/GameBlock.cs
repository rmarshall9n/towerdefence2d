﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace MyTowerDefence
{
    class GameBlock
    {
        public static float LENGTH = 1.0f;
        public static float HALF_LENGTH = 0.5f;
        private Vector3 BOUNDS
        {
            get { return new Vector3(-HALF_LENGTH, -HALF_LENGTH, HALF_LENGTH); }
        }
        
        private Model model;
        private Vector3 position;
        private BoundingBox boundingBox;

        private bool canBuildOn = true;
        private bool containsTower = false;

        public bool CanBuildOn
        {
            get { return canBuildOn; }
        }
        public bool ContainsTower
        {
            get { return containsTower; }
            set { containsTower = value; }
        }

        public Vector3 Position
        {
            get { return position; }
            set 
            {
                position = value;
                boundingBox.Min = position + BOUNDS;
                boundingBox.Max = position - BOUNDS;
            }
        }
        public BoundingBox BoundingBox
        {
            get { return boundingBox; }
        }

        Matrix worldXform;

        public GameBlock(Vector3 position, Model model, bool canBuildOn)
        {
            this.model = model;
            this.position = position;

            boundingBox.Min = position + BOUNDS;
            boundingBox.Max = position - BOUNDS;

            worldXform = Matrix.Identity;
            worldXform *= Matrix.CreateTranslation(position);

            this.canBuildOn = canBuildOn;
            containsTower = false;
        }

        public void Draw(Camera camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.View = camera.ViewXform;
                    effect.Projection = camera.ProjXform;
                    effect.World = transforms[mesh.ParentBone.Index] * worldXform;
                }
                mesh.Draw();
            }
        }
    }
}
