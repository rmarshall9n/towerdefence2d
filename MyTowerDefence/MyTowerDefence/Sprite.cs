﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MyTowerDefence
{
    public abstract class Sprite
    {
        protected Texture2D texture;
        protected Vector2 position;
        protected Vector2 velocity;
        protected Vector2 center;
        private Vector2 origin;
        private float rotation;
        private Rectangle source;

        public Vector2 Position
        {
            get { return position; }
            set { position = value; }
        }
        public Vector2 Center
        {
            get { return center; }
        }
        public Vector2 Origin
        {
            get { return origin; }
        }
        public float Rotation
        {
            get { return rotation; }
            set { rotation = value; }
        }
        public Rectangle Source
        {
            get { return source; }
        }

        public Sprite(Texture2D tex, Vector2 pos)
        {
            texture = tex;
            position = pos;
            velocity = Vector2.Zero;

            center = new Vector2(position.X + texture.Width / 2,
                position.Y + texture.Height / 2);

            origin = new Vector2(texture.Width / 2, texture.Height / 2);

            rotation = 0.0f;
        }

        public virtual void Update(GameTime gameTime)
        {
            source = new Rectangle((int)position.X, (int)position.Y, texture.Width, texture.Height);

            this.center = new Vector2(position.X + texture.Width / 2,
                position.Y + texture.Height / 2);
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(texture, center, null, Color.White, rotation, origin, 1.0f, SpriteEffects.None, 0);
        }

        public virtual void Draw(SpriteBatch spriteBatch, Color colour)
        {
            spriteBatch.Draw(texture, center, null, colour, rotation, origin, 1.0f, SpriteEffects.None, 0);
        }
    }
}
