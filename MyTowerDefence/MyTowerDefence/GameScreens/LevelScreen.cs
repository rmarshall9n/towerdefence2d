﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace MyTowerDefence
{
    class LevelScreen : GameScreen
    {
        Button level1;
        Button level2;
        Button level3;
        Button back;

        public LevelScreen(ScreenManager theScreenManager)
            : base(theScreenManager)
        {
        }

        public override void LoadContent(ContentManager content)
        {
            background = content.Load<Texture2D>(@"UI/splashScreen");

            level1 = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/level1N"),
                        content.Load<Texture2D>(@"UI/Buttons/level1N"),
                        200
                );

            level2 = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/level2N"),
                        content.Load<Texture2D>(@"UI/Buttons/level2N"),
                        level1.Position.Bottom
                );

            level3 = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/level3N"),
                        content.Load<Texture2D>(@"UI/Buttons/level3N"),
                        level2.Position.Bottom
                );

            back = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/backN"),
                        content.Load<Texture2D>(@"UI/Buttons/backH"),
                        level3.Position.Bottom
                );
        }

        public override void HandleInput(InputManager input)
        {
            base.HandleInput(input);

            level1.Update(mouseX, mouseY);
            level2.Update(mouseX, mouseY);
            level3.Update(mouseX, mouseY);
            back.Update(mouseX, mouseY);

            if (input.LeftClick())
            {
                if (level1.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.AddScreen(new GameManager(screenManager, 1));
                    return;
                }
                else if (level2.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.AddScreen(new GameManager(screenManager, 2));
                    return;
                }
                else if (level3.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.AddScreen(new GameManager(screenManager, 3));
                    return;
                }
                else if (back.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.RemoveScreen(this);
                    return;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, Vector2.Zero, Color.White);

            level1.Draw(spriteBatch);
            level2.Draw(spriteBatch);
            level3.Draw(spriteBatch);
            back.Draw(spriteBatch);
        }
    }
}
