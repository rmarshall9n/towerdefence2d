﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace MyTowerDefence
{
    class ControlsScreen : GameScreen
    {
        Texture2D controls;
        Rectangle controlsPos;

        Button back;

        public ControlsScreen(ScreenManager theScreenManager)
            : base(theScreenManager)
        {
        }

        public override void LoadContent(ContentManager content)
        {
            background = content.Load<Texture2D>(@"UI/splashScreen");

            controls = content.Load<Texture2D>(@"UI/controls");
            controlsPos = new Rectangle(640 - (controls.Width / 2), 200, controls.Width, controls.Height);


            back = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/backN"),
                        content.Load<Texture2D>(@"UI/Buttons/backH"),
                        controlsPos.Bottom
                );
        }

        public override void HandleInput(InputManager input)
        {
            base.HandleInput(input);

            back.Update(mouseX, mouseY);

            if (input.LeftClick())
            {
                if (back.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.RemoveScreen(this);
                    return;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, Vector2.Zero, Color.White);

            spriteBatch.Draw(controls, controlsPos, Color.White);

            back.Draw(spriteBatch);
            
        }
    }
}
