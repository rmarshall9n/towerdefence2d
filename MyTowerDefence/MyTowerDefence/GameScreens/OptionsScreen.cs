﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace MyTowerDefence
{
    class OptionsScreen : GameScreen
    {
        Button fullscreen;
        Button controls;
        Button back;

        public OptionsScreen(ScreenManager theScreenManager)
            : base(theScreenManager)
        {
        }

        public override void LoadContent(ContentManager content)
        {
            background = content.Load<Texture2D>(@"UI/splashScreen");

            fullscreen = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/fullscreenN"),
                        content.Load<Texture2D>(@"UI/Buttons/fullscreenH"),
                        200
                );

            controls = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/controlsN"),
                        content.Load<Texture2D>(@"UI/Buttons/controlsH"),
                        fullscreen.Position.Bottom
                );

            back = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/backN"),
                        content.Load<Texture2D>(@"UI/Buttons/backH"),
                        controls.Position.Bottom
                );
        }

        public override void HandleInput(InputManager input)
        {
            base.HandleInput(input);

            fullscreen.Update(mouseX, mouseY);
            controls.Update(mouseX, mouseY);
            back.Update(mouseX, mouseY);

            if (input.LeftClick())
            {
                if (fullscreen.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.Graphics.ToggleFullScreen();
                    return;
                }
                else if (controls.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.AddScreen(new ControlsScreen(screenManager));
                    return;
                }
                else if (back.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.RemoveScreen(this);
                    return;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, Vector2.Zero, Color.White);

            fullscreen.Draw(spriteBatch);
            controls.Draw(spriteBatch);
            back.Draw(spriteBatch);
        }
    }
}
