﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace MyTowerDefence
{
    class MainScreen : GameScreen
    {
        Button play;
        Button level;
        Button options;
        Button quit;

        public MainScreen(ScreenManager theScreenManager)
            : base(theScreenManager)
        {
        }

        public override void LoadContent(ContentManager content)
        {           
            background = content.Load<Texture2D>(@"UI/splashScreen");

            play = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/playN"),
                        content.Load<Texture2D>(@"UI/Buttons/playH"),
                        200
                );

            level = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/levelN"),
                        content.Load<Texture2D>(@"UI/Buttons/levelH"),
                        play.Position.Bottom
                );

            options = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/optionsN"),
                        content.Load<Texture2D>(@"UI/Buttons/optionsH"),
                        level.Position.Bottom
                );

            quit = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/quitN"),
                        content.Load<Texture2D>(@"UI/Buttons/quitH"),
                        options.Position.Bottom
                );

            MediaPlayer.IsRepeating = true;
            MediaPlayer.Play(content.Load<Song>("Sounds/Music"));
            
        }

        public override void HandleInput(InputManager input)
        {
            base.HandleInput(input);

            play.Update(mouseX, mouseY);
            level.Update(mouseX, mouseY);
            options.Update(mouseX, mouseY);
            quit.Update(mouseX, mouseY);

            if (input.LeftClick())
            {
                if (play.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.AddScreen(new GameManager(screenManager, 1));
                    return;
                }
                else if (level.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.AddScreen(new LevelScreen(screenManager));
                    return;
                }
                else if (options.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.AddScreen(new OptionsScreen(screenManager));
                    return;
                }
                else if (quit.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.ExitGame();
                    return;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, Vector2.Zero, Color.White);

            play.Draw(spriteBatch);
            level.Draw(spriteBatch);
            options.Draw(spriteBatch);
            quit.Draw(spriteBatch);
        }
    }
}
