﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MyTowerDefence
{
    class PauseScreen : GameScreen
    {
        Button resume;
        Button options;
        Button quit;

        GameScreen currentScreen;
        
        public PauseScreen(ScreenManager theScreenManager, GameScreen current)
            : base(theScreenManager)
        {
            popUp = true;
            currentScreen = current;
        }

        public override void LoadContent(Microsoft.Xna.Framework.Content.ContentManager content)
        {
            background = content.Load<Texture2D>(@"UI/pauseScreen");

            resume = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/resumeN"),
                        content.Load<Texture2D>(@"UI/Buttons/resumeH"),
                        300
                );

            options = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/optionsN"),
                        content.Load<Texture2D>(@"UI/Buttons/optionsH"),
                        resume.Position.Bottom
                );

            quit = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/quitN"),
                        content.Load<Texture2D>(@"UI/Buttons/quitH"),
                        options.Position.Bottom
                );
        }

        public override void HandleInput(InputManager input)
        {
            base.HandleInput(input);

            resume.Update(mouseX, mouseY);
            options.Update(mouseX, mouseY);
            quit.Update(mouseX, mouseY);

            if (input.LeftClick())
            {
                if (resume.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.RemoveScreen(this);
                    return;
                }
                else if (options.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.AddScreen(new OptionsScreen(screenManager));
                    return;
                }
                else if (quit.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.RemoveScreen(this);
                    ScreenManager.RemoveScreen(currentScreen);
                    return;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, Vector2.Zero, Color.White);

            resume.Draw(spriteBatch);
            options.Draw(spriteBatch);
            quit.Draw(spriteBatch);
        }
    }
}
