﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace MyTowerDefence
{
    class GameCompletesScreen : GameScreen
    {
        Button complete;
        Button quit;

        public GameCompletesScreen(ScreenManager theScreenManager)
            : base(theScreenManager)
        {
        }

        public override void LoadContent(ContentManager content)
        {
            background = content.Load<Texture2D>(@"UI/splashScreen");

            complete = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/completeN"),
                        content.Load<Texture2D>(@"UI/Buttons/completeN"),
                        200
                );

            quit = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/quitN"),
                        content.Load<Texture2D>(@"UI/Buttons/quitH"),
                        complete.Position.Bottom
                );
        }

        public override void HandleInput(InputManager input)
        {
            base.HandleInput(input);

            quit.Update(mouseX, mouseY);

            if (input.LeftClick())
            {
                if (quit.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.RemoveScreen(this);
                    return;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, Vector2.Zero, Color.White);

            complete.Draw(spriteBatch);
            quit.Draw(spriteBatch);
        }
    }
}
