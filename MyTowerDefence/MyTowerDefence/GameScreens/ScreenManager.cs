﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace MyTowerDefence
{
    public class ScreenManager : DrawableGameComponent
    {
        private SpriteBatch spriteBatch;
        private ContentManager content;
        private InputManager input;
        private GraphicsDeviceManager graphics;
        private List<GameScreen> gameScreens = new List<GameScreen>();
        public List<GameScreen> updatingScreens = new List<GameScreen>();

        public GraphicsDeviceManager Graphics
        {
            get { return graphics; }
        }


        public ScreenManager(Game game, GraphicsDeviceManager graphicsManager)
            : base(game)
        {
            content = Game.Content;
            input = new InputManager();
            graphics = graphicsManager;
        }

        protected override void LoadContent()
        {
            spriteBatch = new SpriteBatch(GraphicsDevice);
            
            AddScreen(new GameManager(this,1));
            //AddScreen(new MainScreen(this));
            
            base.LoadContent();
        }

        public override void Update(GameTime gameTime)
        {
            input.Update();
            GameScreen screen = gameScreens[gameScreens.Count - 1];

            screen.Update(gameTime);
            screen.HandleInput(input);

            //! remove
            /*
            updatingScreens.Clear();

            foreach (GameScreen g in gameScreens)
                updatingScreens.Add(g);

            while (updatingScreens.Count > 0)
            {
                GameScreen screen = updatingScreens[updatingScreens.Count - 1];

                updatingScreens.Remove(screen);

                screen.Update(gameTime);

                if (screen.Active)
                    screen.HandleInput(input);
            }
            */

            base.Update(gameTime);
        }

        public override void Draw(GameTime gameTime)
        {
            // draw sprites
            GraphicsDevice.BlendState = BlendState.Opaque;
            GraphicsDevice.DepthStencilState = DepthStencilState.Default;
            GraphicsDevice.RasterizerState = RasterizerState.CullCounterClockwise;
            GraphicsDevice.SamplerStates[0] = SamplerState.LinearWrap;

            // Draw Models
            if (gameScreens.Count > 1 && gameScreens[gameScreens.Count - 1].PopUp)
                gameScreens[gameScreens.Count - 2].DrawModels();

            gameScreens[gameScreens.Count - 1].DrawModels();

            // draw sprites
            spriteBatch.Begin();

            if (gameScreens.Count > 1 && gameScreens[gameScreens.Count - 1].PopUp)
                gameScreens[gameScreens.Count - 2].Draw(spriteBatch);

            gameScreens[gameScreens.Count - 1].Draw(spriteBatch);

            spriteBatch.End();
            
            base.Draw(gameTime);
        }

        public void AddScreen(GameScreen gameScreen)
        {
            gameScreen.ScreenManager = this;
            gameScreen.LoadContent(content);

            foreach (GameScreen g in gameScreens)
                g.Active = false;

            gameScreen.Active = true;

            gameScreens.Add(gameScreen);
        }

        public void RemoveScreen(GameScreen gameScreen)
        {
            gameScreens.Remove(gameScreen);
            updatingScreens.Remove(gameScreen);

            if(gameScreens.Count >= 1)
                gameScreens[gameScreens.Count -1].Active = true;
        }

        public void ExitGame()
        {
            Game.Exit();
        }
    }
}
