﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace MyTowerDefence
{
    class GameOverScreen : GameScreen
    {
        Button retry;
        Button quit;
        int currentLevel;

        public GameOverScreen(ScreenManager theScreenManager, int level)
            : base(theScreenManager)
        {
            currentLevel = level;
        }

        public override void LoadContent(ContentManager content)
        {
            background = content.Load<Texture2D>(@"UI/splashScreen");

            retry = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/retryN"),
                        content.Load<Texture2D>(@"UI/Buttons/retryH"),
                        200
                );

            quit = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/quitN"),
                        content.Load<Texture2D>(@"UI/Buttons/quitH"),
                        retry.Position.Bottom
                );
        }

        public override void HandleInput(InputManager input)
        {
            base.HandleInput(input);

            retry.Update(mouseX, mouseY);
            quit.Update(mouseX, mouseY);

            if (input.LeftClick())
            {
                if (retry.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.RemoveScreen(this);
                    screenManager.AddScreen(new GameManager(screenManager, currentLevel));

                    return;
                }
                else if (quit.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.ExitGame();
                    return;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, Vector2.Zero, Color.White);

            retry.Draw(spriteBatch);
            quit.Draw(spriteBatch);
        }
    }
}
