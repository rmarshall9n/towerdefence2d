﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace MyTowerDefence
{
    class LevelCompleteScreen : GameScreen
    {
        Button nextLevel;
        Button quit;
        int currentLevel;

        public LevelCompleteScreen(ScreenManager theScreenManager, int theCurrentLevel)
            : base(theScreenManager)
        {
            currentLevel = theCurrentLevel;
        }

        public override void LoadContent(ContentManager content)
        {
            background = content.Load<Texture2D>(@"UI/splashScreen");

            nextLevel = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/nextN"),
                        content.Load<Texture2D>(@"UI/Buttons/nextH"),
                        200
                );

            quit = new Button(
                        content.Load<Texture2D>(@"UI/Buttons/quitN"),
                        content.Load<Texture2D>(@"UI/Buttons/quitH"),
                        nextLevel.Position.Bottom
                );
        }

        public override void HandleInput(InputManager input)
        {
            base.HandleInput(input);

            nextLevel.Update(mouseX, mouseY);
            quit.Update(mouseX, mouseY);

            if (input.LeftClick())
            {
                if (nextLevel.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.RemoveScreen(this);
                    screenManager.AddScreen(new GameManager(ScreenManager, currentLevel + 1));
                    return;
                }
                else if (quit.Hovering)
                {
                    Game.soundManager.playSound("beep");
                    screenManager.ExitGame();
                    return;
                }
            }
        }

        public override void Draw(SpriteBatch spriteBatch)
        {
            spriteBatch.Draw(background, Vector2.Zero, Color.White);

            nextLevel.Draw(spriteBatch);
            quit.Draw(spriteBatch);
        }
    }
}
