﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace MyTowerDefence
{
    public abstract class GameScreen
    {
        protected Texture2D background;

        protected ScreenManager screenManager;
        private bool active = false;
        protected bool popUp = false;

        protected int mouseX, mouseY;

        public ScreenManager ScreenManager
        {
            get { return screenManager; }
            set { screenManager = value; }
        }

        public bool Active
        {
            get { return active; }
            set { active = value; }
        }

        public bool PopUp
        {
            get { return popUp; }
        }

        public GameScreen(ScreenManager theScreenManager)
        {
            screenManager = theScreenManager;
        }

        public virtual void LoadContent(ContentManager content)
        {

        }

        public virtual void Update(GameTime gameTime)
        {
            
        }

        public virtual void HandleInput(InputManager input)
        {
            mouseX = (int)input.MousePosition.X;
            mouseY = (int)input.MousePosition.Y;
        }

        public virtual void Draw(SpriteBatch spriteBatch)
        {

        }
        public virtual void DrawModels()
        {

        }
    }
}
