﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.IO;
using System.Xml.Serialization;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Graphics;

/*-----------------------------------
 * Maps must be created as .txt
 * Maps must be 23 x 40 seperated by a single comma ,
 * Each map layout must be seperated by a single return character \n
 * Way points should be listed on seperate lines in the format x,y
 * tile, overlay, collision, waypoints
 * ---------------------------------- */

namespace MyTowerDefence
{
    class Level
    {
        private int levelNumber;

        private List<GameBlock> gameBlocks;
        private Queue<Vector3> waypoints = new Queue<Vector3>();

        private Model boxModel;
        private Model grassModel;

        public List<GameBlock> GameBlocks
        {
            get { return gameBlocks; }
        }
        public Queue<Vector3> Waypoints
        {
            get { return waypoints; }
        }

        public Level(int level)
        {
            levelNumber = level;
        }

        public void LoadContent(ContentManager content)
        {
            boxModel = content.Load<Model>(@"Models/Box/box1");
            grassModel = content.Load<Model>(@"Models/Box/box2");

            LoadLevel();
        }

        public void Draw(Camera camera)
        {
            foreach (GameBlock block in gameBlocks)
            {
                block.Draw(camera);
            }
        }

        /*
        public void SaveLevel()
        {
            TextWriter writer = new StreamWriter("map1.txt", false);

            // tile map
            for (int y = 0; y < Game.MAP_HEIGHT; y++)
            {
                writer.Flush();
                string line = "";

                for (int x = 0; x < Game.MAP_WIDTH; x++)
                {
                    line += tileMap[y, x].ToString() + ',';
                }

                writer.Write(line + "\n");
            }
            writer.WriteLine();


            // overlay map
            for (int y = 0; y < Game.MAP_HEIGHT; y++)
            {
                writer.Flush();
                string line = "";

                for (int x = 0; x < Game.MAP_WIDTH; x++)
                {
                    line += overlayMap[y, x].ToString() + ',';
                }

                writer.Write(line + "\n");
            }
            writer.WriteLine();


            // collision map
            for (int y = 0; y < Game.MAP_HEIGHT; y++)
            {
                writer.Flush();
                string line = "";

                for (int x = 0; x < Game.MAP_WIDTH; x++)
                {
                    line += collisionMap[y, x].ToString() + ',';
                }

                writer.Write(line + "\n");
            }
            writer.WriteLine();


            // waypoints
            foreach (Vector2 p in waypoints)
            {
                writer.Write((p.X / Game.TILE_SIZE).ToString() + ',' + (p.Y / Game.TILE_SIZE).ToString() + '\n');
            }


            writer.Close();
            writer.Dispose();
        }
        */
        public void LoadLevel()
        {
            // open a stream to read the map data
            StreamReader reader = new StreamReader("Content/Maps/map" + levelNumber.ToString() + ".txt");
            string line = "";

            #region Load Map Tiles
            gameBlocks = new List<GameBlock>();
            for (int y = 0; y < Game.MAP_HEIGHT; y++)
            {
                line = reader.ReadLine();
                string[] tilesInLine = line.Split(',');
                
                for (int x = 0; x < Game.MAP_WIDTH; x++)
                {
                    Vector3 pos = new Vector3(x * GameBlock.LENGTH, -GameBlock.HALF_LENGTH, y * GameBlock.LENGTH);

                    switch (tilesInLine[x])
                    {
                        case "0":
                            pos.Y = -0.2f;
                            gameBlocks.Add(new GameBlock(pos, grassModel, true));
                            break;

                        case "1":
                            gameBlocks.Add(new GameBlock(pos, boxModel, false));
                            break;

                        default:
                            break;
                    }
                }
            }
            reader.ReadLine(); // read empty line between maps
            #endregion

            #region Load Waypoints
            waypoints = new Queue<Vector3>();
            line = reader.ReadLine();

            while (!string.IsNullOrEmpty(line))
            {
                string[] points = line.Split(',');

                int x, y, z;

                int.TryParse(points[0], out x);
                int.TryParse(points[1], out y);
                int.TryParse(points[2], out z);

                waypoints.Enqueue(new
                    Vector3(x * GameBlock.LENGTH, 0, z * GameBlock.LENGTH));

                line = reader.ReadLine();
            }
            #endregion

            /*
            // overlay map
            overlayMap = new int[Game.MAP_HEIGHT, Game.MAP_WIDTH];

            for (int y = 0; y < Game.MAP_HEIGHT; y++)
            {
                line = reader.ReadLine();
                string[] tilesInLine = line.Split(',');

                for (int x = 0; x < Game.MAP_WIDTH; x++)
                {
                    int.TryParse(tilesInLine[x], out overlayMap[y, x]);
                }
            }

            reader.ReadLine(); // read empty line between maps
 
             * */

            // close the stream
            reader.Close();
            reader.Dispose();
             
        }
    }
}
