﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;

namespace MyTowerDefence
{
    class WaveManager
    {
        Model skeletonModel;

        private ScreenManager screenManager;
        private Level level;

        private Wave currentWave;
        private int waveNumber = 0;
        private int numOfWaves;

        public List<Enemy> Enemies
        {
            get { return currentWave.Enemies; }
        }
        public int WaveNumber
        {
            get { return waveNumber; }
        }

        


        //private ParticleEmitter explosionEmitter;
        private bool noMoreWaves = false;
        public bool NoMoreWaves
        {
            get { return noMoreWaves; }
        }
        


        public WaveManager(ScreenManager screenManager, Level level, int numOfWaves)
        {
            this.screenManager = screenManager;
            this.level = level;
            this.numOfWaves = numOfWaves;
        }

        public void LoadContent(ContentManager content)
        {
            skeletonModel = content.Load<Model>(@"Models/Monsters/Skeleton/bones_all");
            SpawnNextWave();
        }

        public void Update(GameTime gameTime, Player player)
        {
            if (waveNumber > numOfWaves)
                noMoreWaves = true;

            // spawn the next wave
            if (currentWave.WaveOver)
                SpawnNextWave();

            currentWave.Update(gameTime, player);
        }

        public void Draw(Camera camera)
        {
            currentWave.Draw(camera);
        }

        private void SpawnNextWave()
        {
            // should change to suit multipe levels later in development
            waveNumber++;
            Enemy nextEnemy = new Enemy(level.Waypoints.Peek(),Vector3.Zero, 0.7f, skeletonModel, 100000, 0.1f, 0);
            nextEnemy.SetWayPoints(level.Waypoints);
            int numToSpawn = 5;

            switch (waveNumber)
            {                                                              // hp / speed / money
                case 1: // normal
                    //nextEnemy = new Enemy(level.Waypoints.Peek(), Vector3.Zero, 0.5f, skeletonModel, 2, 0.05f, 0);
                    numToSpawn = 3;
                    break;
                    
                case 2: // fast
                    //nextEnemy = new Enemy(level.Waypoints.Peek(), Vector3.Zero, 0.5f, skeletonModel, 2, 0.05f, 0);
                    numToSpawn = 5;
                    break;

                case 3: // tough
                    //nextEnemy = new Enemy(level.Waypoints.Peek(), Vector3.Zero, 0.5f, skeletonModel, 2, 0.05f, 0);
                    numToSpawn = 3;
                    break;

                case 4: // boss
                    nextEnemy = new Enemy(level.Waypoints.Peek(), Vector3.Zero, 0.5f, skeletonModel, 2, 0.05f, 0);
                    numToSpawn = 1;
                    break;

                case 5: // boss
                    nextEnemy = new Enemy(level.Waypoints.Peek(), Vector3.Zero, 0.5f, skeletonModel, 2, 0.05f, 0);
                    numToSpawn = 1;
                    break;

                default:
                    nextEnemy = new Enemy(level.Waypoints.Peek(), Vector3.Zero, 0.5f, skeletonModel, 2, 0.05f, 0);
                    break;
            }

            currentWave = new Wave(nextEnemy, numToSpawn, level);
        }
    }
}
