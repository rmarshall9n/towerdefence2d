﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace MyTowerDefence
{
    class Player
    {
        private int money;
        private int lives;
        private List<Tower> towerList = new List<Tower>();

        public int Money
        {
            get { return money; }
            set { money = value; }
        }
        public int Lives
        {
            get { return lives; }
            set { lives = value; }
        }
        public List<Tower> TowerList
        {
            get { return towerList; }
            set { towerList = value; }
        }

        public void SellTower(Tower selectedTower)
        {
            towerList.Remove(selectedTower);

            money += selectedTower.Cost;
        }

        public Player()
        {
            lives = 10;
            money = 19000;//190
        }
    }
}
