﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MyTowerDefence
{
    public class ParticleEmitter
    {
        private Random random;
        private Vector2 emitterLocation;

        public Vector2 EmitterLocation
        {
            get { return emitterLocation; }
            set { emitterLocation = value; }
        }

        public float SetX
        {
            get { return emitterLocation.X; }
            set { emitterLocation.X = value; }
        }

        private List<Particle> particles;
        private List<Texture2D> textures;

        public ParticleEmitter(List<Texture2D> tex, Vector2 loc)
        {
            EmitterLocation = loc;
            textures = tex;
            particles = new List<Particle>();
            random = new Random();
        }

        public void Update(bool randomColor)
        {
            int total = 10;

            for (int i = 0; i < total; i++)
            {
                particles.Add(GenerateNewParticle(randomColor));
            }

            for (int particle = 0; particle < particles.Count; particle++)
            {
                particles[particle].Update();
                if (particles[particle].TTL <= 0)
                {
                    particles.RemoveAt(particle);
                    particle--;
                }
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            for (int i = 0; i < particles.Count; i++)
            {
                particles[i].Draw(spriteBatch);
            }
        }

        private Particle GenerateNewParticle(bool randomColor)
        {
            Texture2D texture = textures[random.Next(textures.Count)];
            Vector2 position = EmitterLocation;

            Vector2 velocity = new Vector2(
                1.0f * (float)(random.NextDouble() * 2 - 1),
                1.0f * (float)(random.NextDouble() * 2 - 1));

            float angle = 0;

            float angularVelocity = 0.1f * (float)(random.NextDouble() * 2 - 1);
            
            Color color = Color.White;
            if (randomColor == true)
            {
                color = new Color(
                    (float)random.NextDouble(),
                    (float)random.NextDouble(),
                    (float)random.NextDouble());
            }


            float size = (float)random.NextDouble();

            int ttl = 20 + random.Next(40);

            return new Particle(texture, position, velocity, angle, angularVelocity, color, size, ttl);
        }

    }
}
