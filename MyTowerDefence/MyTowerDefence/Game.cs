using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Audio;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.GamerServices;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;
using Microsoft.Xna.Framework.Media;

namespace MyTowerDefence
{
    public class Game : Microsoft.Xna.Framework.Game
    {
        public static int TILE_SIZE = 32;
        public static int BLOCK_SAFE_OFFSET = 19;
        public static int MAP_HEIGHT = 40;
        public static int MAP_WIDTH = 50;

        private static int screenWidth = 1280;
        private static int screenHeight = 720;

        public static int SCREEN_WIDTH
        {
            get { return screenWidth; }
        }
        public static int SCREEN_HEIGHT
        {
            get { return screenHeight; }
        }

        GraphicsDeviceManager graphics;
        ScreenManager screenManager;

        public static Sound soundManager;
        
        public Game()
        {
            graphics = new GraphicsDeviceManager(this);
            graphics.PreferredBackBufferWidth = screenWidth;
            graphics.PreferredBackBufferHeight = screenHeight;

            IsMouseVisible = true;
            
            Content.RootDirectory = "Content";
            
            soundManager = new Sound();

            screenManager = new ScreenManager(this, graphics);
            Components.Add(screenManager);
        }

        public void ToggleFullscreen()
        {
            if (graphics.IsFullScreen)
            {
                graphics.IsFullScreen = false;
                graphics.PreferredBackBufferWidth = screenWidth;
                graphics.PreferredBackBufferHeight = screenHeight;
            }
            else
            {
                graphics.IsFullScreen = true;
                screenWidth = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Width;
                screenHeight = GraphicsAdapter.DefaultAdapter.CurrentDisplayMode.Height;
                graphics.PreferredBackBufferWidth = screenWidth;
                graphics.PreferredBackBufferHeight = screenHeight;
                graphics.ApplyChanges();
            }
        }

        protected override void Initialize()
        {
            //ToggleFullscreen();
            base.Initialize();
        }

        protected override void LoadContent()
        {
            soundManager.LoadContent(Content);
        }

        protected override void Update(GameTime gameTime)
        {
            base.Update(gameTime);
        }

        protected override void Draw(GameTime gameTime)
        {
            GraphicsDevice.Clear(Color.DarkOliveGreen);
                        
            base.Draw(gameTime);
        }
    }
}
