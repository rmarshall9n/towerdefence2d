﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MyTowerDefence
{
    class Bullet : GameModel
    {
        private Vector3 velocity;
        private float speed;

        public Bullet(Vector3 position, Vector3 rotation, float scale, Model model)
            : base(position, rotation, scale, model)
        {
            speed = 0.1f;
        }

        public override void Update(GameTime gameTime)
        {
            position += velocity;
            
            base.Update(gameTime);
        }

        public void SetVelocity(Vector3 targetPosition)
        {
            Vector3 direction = new Vector3(targetPosition.X - position.X, 0, targetPosition.Z - position.Z);
            direction.Normalize();

            velocity = Vector3.Multiply(direction, speed);
        }

        public override void Draw(Camera camera)
        {
            base.Draw(camera);
        }
    }
}
