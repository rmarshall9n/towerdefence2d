﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Input;

namespace MyTowerDefence
{
    class Button
    {
        private Texture2D normal;
        private Texture2D hover;
        private Rectangle position;
        private Color colour;

        private bool hovering;

        public Rectangle Position
        {
            get { return position; }
        }
        public Texture2D Texture
        {
            get { return normal; }
            set { normal = value; }
        }

        public bool Hovering
        {
            get { return hovering; }
        }

        // takes a custom position
        public Button(Texture2D normalTex, Texture2D hoverTex, int x, int y)
        {
            normal = normalTex;
            hover = hoverTex;
            position = new Rectangle(x, y, normal.Width, normal.Height);

            hovering = false;
        }

        public Button(Texture2D normalTex, Texture2D hoverTex, int x, int y, Color c)
        {
            normal = normalTex;
            hover = hoverTex;
            position = new Rectangle(x, y, normal.Width, normal.Height);

            hovering = false;

            colour = c;
        }

        // centers the button along the x axis, custom y
        public Button(Texture2D normalTex, Texture2D hoverTex, int y)
        {
            normal = normalTex;
            hover = hoverTex;
            position = new Rectangle(Game.SCREEN_WIDTH / 2 - normal.Width / 2, y, normal.Width, normal.Height);
            hovering = false;
        }

        public void Update(int mouseX, int mouseY)
        {
            hovering = false;

            if (position.Contains(mouseX, mouseY))
            {
                hovering = true;
            }
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            if (hovering)
            {
                if (hover == null)
                    spriteBatch.Draw(normal, position, colour);
                else
                    spriteBatch.Draw(hover, position, Color.White);
            }
            else
            {
                spriteBatch.Draw(normal, position, Color.White);
            }
        }
    }
}
