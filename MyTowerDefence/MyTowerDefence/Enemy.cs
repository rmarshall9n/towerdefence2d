﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MyTowerDefence
{
    public class Enemy : GameModel
    {
        private float health;
        private float speed;
        private int money;
        private bool alive;
        private bool pathComplete;

        private bool slowed, poisoned, splashed;
        private float splashDamage, splashRadius;
        private float slowSpeed, initialSpeed, slowDur = 0;
        private float poisonDPS, poisonTickChecker, poisonDur = 0;
        private Color effectColour;

        private Queue<Vector3> waypoints = new Queue<Vector3>();

        public float Health
        {
            get { return health; }
            set { health = value; }
        }
        public float Speed
        {
            get { return speed; }
        }
        public int MoneyGiven
        {
            get { return money; }
        }
        public bool IsDead
        {
            get { return !alive; }
            set { alive = !value; }
        }
        public bool PathComplete
        {
            get { return pathComplete; }
        }
        public bool Splashed
        {
            get { return splashed; }
            set { splashed = value; }
        }

        private float rotationTime;

        public Enemy(Vector3 position, Vector3 rotation, float scale, Model model, float hp, float spd, int moneyGiven)
            : base(position, rotation, scale, model)
        {
            initialSpeed = spd;
            slowed = poisoned = false;
            effectColour = Color.White;

            health = hp;
            speed = spd;
            money = moneyGiven;
            alive = true;
            rotationTime = 0;
        }

        public void Update(GameTime gameTime)
        {
            HandleEffects(gameTime);

            // kill if no health
            if (health <= 0)
                alive = false;

            // transverse through waypoints unless complte the path
            if (waypoints.Count > 0)
            {
                // if enemy is clost to the peek position, set next waypoint
                if (DistanceToWaypoint() < speed)
                {
                    position = waypoints.Peek();
                    waypoints.Dequeue();
                }
                else
                {
                    // vector arithmetic to calculate velocity and move enemy through map
                    Vector3 direction = waypoints.Peek() - position;
                    direction.Normalize();
                    float desiredY = (float)Math.Atan2(direction.X, direction.Z);

                    if(Rotation.Y != desiredY)
                    {
                        rotationTime += (float) gameTime.ElapsedGameTime.Milliseconds / 1000;

                        Rotation = new Vector3(Rotation.X, 
                            MathHelper.Lerp(Rotation.Y, desiredY, rotationTime),
                            Rotation.Z);

                        if (rotationTime >= 1)
                        {
                            Rotation = new Vector3(Rotation.X, Rotation.Y, Rotation.Z);
                            rotationTime = 0;
                        }
                    }
                    else
                    {
                        Vector3 velocity = Vector3.Multiply(direction, speed);
                        position += velocity;
                    }
                }
            }
            else
            {
                alive = false;
                pathComplete = true;
            }

            base.Update(gameTime);
        }

        public void Draw(Camera camera)
        {
            if(alive)
            {
                base.Draw(camera);
            } 
        }

        public void SetWayPoints(Queue<Vector3> newWaypoints)
        {
            waypoints.Clear();
            foreach (Vector3 w in newWaypoints)
                waypoints.Enqueue(w);

            position = waypoints.Dequeue();
        }

        public float DistanceToWaypoint()
        {
            return Vector3.Distance(position, waypoints.Peek());
        }

        public void Poisoned(int totalDamage, float durationSeconds)
        {
            poisoned = true;
            poisonDPS = totalDamage / durationSeconds;
            poisonDur = durationSeconds * 1000;
            poisonTickChecker = poisonDur - 1000;
        }

        public void Slowed(int percent, float durationSeconds)
        {
            slowed = true;
            slowSpeed = initialSpeed - initialSpeed * (percent / 100.0f);
            slowDur = durationSeconds * 1000;
        }

        public void Splash(int damage, float radius)
        {
            splashed = true;
            splashDamage = damage;
            splashRadius = radius;
        }

        private void HandleEffects(GameTime gameTime)
        {
            // handle slow effect
            if (slowed)
            {
                if (slowDur > 0)
                {
                    slowDur -= gameTime.ElapsedGameTime.Milliseconds;
                    speed = slowSpeed;
                    effectColour = Color.Blue;
                }
                else
                {
                    speed = initialSpeed;
                    effectColour = Color.White;
                    slowed = false;
                }
            }

            // handle poison effect
            if (poisoned)
            {
                if (poisonDur > 0)
                {
                    effectColour = Color.Green;
                    poisonDur -= gameTime.ElapsedGameTime.Milliseconds;

                    if (poisonDur <= poisonTickChecker)
                    {
                        poisonTickChecker -= 1000;
                        health -= poisonDPS;
                        effectColour = Color.Red;
                    }
                }
                else
                {
                    effectColour = Color.White;
                    poisoned = false;
                }
            }
            // if both poisoned and slowed, colour yellow
            if (slowed && poisoned)
            {
                effectColour = Color.Yellow;
            }
        }

        public void HandleSplash(List<Enemy> enemyList)
        {
            foreach (Enemy e in enemyList)
            {
                if (Vector3.Distance(position, e.Position) < splashRadius)
                {
                    e.Health -= splashDamage;
                    splashed = false;
                }
            }
        }
    }
}
