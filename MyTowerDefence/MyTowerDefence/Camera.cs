﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MyTowerDefence
{
    public class Camera
    {
        private Matrix viewXform;
        private Matrix projXform;

        private Vector3 position;
        private Vector3 direction;

        private float currentYaw = 0;

        public Matrix ViewXform
        {
            get { return viewXform; }
        }
        public Matrix ProjXform
        {
            get { return projXform; }
        }
        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }
        
        public Camera(Vector3 position, Vector3 at, Vector2 screenBounds)
        {
            this.position = position;
            this.direction = at - position;
            direction.Normalize();

            viewXform = Matrix.CreateLookAt(position, position + direction, Vector3.Up);

            projXform = Matrix.CreatePerspectiveFieldOfView(MathHelper.PiOver4,
                screenBounds.X / screenBounds.Y,
                1.0f, 1000.0f);
        }

        public void Update()
        {
            // keep the camera oriented in the right direction / position
            viewXform = Matrix.CreateLookAt(position, position + direction, Vector3.Up);
        }

        // DIRECTIONAL MOVEMENT
        public void MoveUp()
        {
            if(position.Z > Game.BLOCK_SAFE_OFFSET + 2)
                position -= new Vector3(0,0,1) * 0.2f;
        }
        public void MoveDown()
        {
            if(position.Z < Game.MAP_HEIGHT - 1)
                position += new Vector3(0,0,1) * 0.2f;
        }

        public void MoveLeft()
        {
            if(position.X > Game.BLOCK_SAFE_OFFSET)
                position -= new Vector3(1,0,0) * 0.2f;
        }
        public void MoveRight()
        {
            if(position.X < Game.MAP_WIDTH - Game.BLOCK_SAFE_OFFSET - 1)
                position += new Vector3(1, 0, 0) * 0.2f;
        }

        // ROTATION
        public void RotateYaw(float amount)
        {
            float angle = (-MathHelper.PiOver4 / 150) * amount;

            if (Math.Abs(currentYaw + angle) < MathHelper.PiOver4 * 0.5f)
            {
                direction = Vector3.Transform(direction,
                    Matrix.CreateFromAxisAngle(Vector3.Up, (-MathHelper.PiOver4 / 150) * amount));

                currentYaw += angle;
            }
        }

        // CAST A RAY FROM CAMERA
        public Ray GetMouseRay(Vector2 position, Viewport viewport)
        {
            Vector3 nearPoint = new Vector3(position.X, position.Y, 0);
            Vector3 farPoint = new Vector3(position.X, position.Y, 1);

            nearPoint = viewport.Unproject(nearPoint, projXform, viewXform, Matrix.Identity);
            farPoint = viewport.Unproject(farPoint, projXform, viewXform, Matrix.Identity);

            Vector3 rayDir = farPoint - nearPoint;
            rayDir.Normalize();

            return new Ray(nearPoint, rayDir);
        }
    }
}
