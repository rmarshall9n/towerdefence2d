﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MyTowerDefence
{
    class Wave
    {
        int numEnemiesThisRound;
        int numEnemiesSpawned = 0;

        float spawnTimer;
        bool waveOver = false;
        
        Enemy waveEnemy;
        List<Enemy> enemies = new List<Enemy>();
        Level level;

        public bool WaveOver
        {
            get { return waveOver; }
        }

        public List<Enemy> Enemies
        {
            get { return enemies; }
        }

        public Wave(Enemy enemy, int numEnemies, Level level)
        {
            waveEnemy = enemy;
            numEnemiesThisRound = numEnemies;
            this.level = level;
        }

        public void Update(GameTime gameTime, Player player)
        {
            // spawn an initial enemy
            if (numEnemiesSpawned == 0 && waveOver == false)
                SpawnEnemy();

            // have a delay between spawned enemies
            if (numEnemiesSpawned < numEnemiesThisRound)
            {
                spawnTimer += (float)gameTime.ElapsedGameTime.TotalMilliseconds;

                if (spawnTimer >= 100 / waveEnemy.Speed) //if (spawnTimer >= 600 / waveEnemy.Speed)
                {
                    SpawnEnemy();
                }
            }

            // update enemies, remove if dead and check if wave over
            if (enemies.Count > 0)
            {
                for (int i = 0; i < enemies.Count; i++)
                {
                    enemies[i].Update(gameTime);

                    if (enemies[i].PathComplete)
                        player.Lives--;

                    if (enemies[i].IsDead)
                    {
                        player.Money += enemies[i].MoneyGiven;
                        enemies.Remove(enemies[i]);
                    }
                    // check if no enemies left and all enemies have been spawned
                    if (enemies.Count == 0 && numEnemiesSpawned == numEnemiesThisRound)
                        waveOver = true;
                }
            }
        }

        public void Draw(Camera camera)
        {
            if (enemies.Count > 0)
            {
                foreach (Enemy e in enemies)
                    e.Draw(camera);
            }
        }

        public void SpawnEnemy()
        {
            Enemy e = new Enemy(waveEnemy.Position, waveEnemy.Rotation, waveEnemy.Scale, waveEnemy.Model, waveEnemy.Health, waveEnemy.Speed, waveEnemy.MoneyGiven);
            e.SetWayPoints(level.Waypoints);
            enemies.Add(e);

            spawnTimer = 0;
            numEnemiesSpawned++;
        }
    }
}
