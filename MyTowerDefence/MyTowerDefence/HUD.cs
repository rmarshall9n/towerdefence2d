﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

namespace MyTowerDefence
{
    public enum TowerType { fast = 0, slow = 1, splash = 2, poison = 3, none };

    class HUD
    {
        private GameManager gameManager;
        private SpriteFont arial;
        private Texture2D background;
        private Texture2D[] towerTextures;

        private Vector2 hudPos;

        private Button[] towerButtonList;
        private Button upgrade;
        private Button sell;

        public HUD(GameManager gameManager)
        {
            this.gameManager = gameManager;
        }

        public void LoadContent(ContentManager content)
        {
            // load fonts
            arial = content.Load<SpriteFont>(@"Fonts/Arial");
            
            // load textures
            background = content.Load<Texture2D>(@"UI/HUD");
            Texture2D upgradeArrow = content.Load<Texture2D>(@"UI/upgradeArrow");
            Texture2D sellButton = content.Load<Texture2D>(@"UI/sellButton");

            // fast = 0, slow = 1, splash = 2, poison = 3
            towerTextures = new Texture2D[4]
            {
                content.Load<Texture2D>(@"Towers/towerFast"),
                content.Load<Texture2D>(@"Towers/towerSlow"),
                content.Load<Texture2D>(@"Towers/towerSplash"),
                content.Load<Texture2D>(@"Towers/towerPoison")
            };

            // hud offset
            hudPos = new Vector2(Game.SCREEN_WIDTH / 2 - background.Width / 2, Game.SCREEN_HEIGHT - background.Height);

            // create buttons
            upgrade = new Button(upgradeArrow, null, (int)hudPos.X + 64, (int)hudPos.Y + 50, Color.Gray);
            sell = new Button(sellButton, null, (int)hudPos.X + 64 * 4, (int)hudPos.Y + 50, Color.Gray);

            // create tower buttons
            towerButtonList = new Button[towerTextures.Length];

            for (int i = 0; i < towerButtonList.Length; i++)
                towerButtonList[i] = new Button(towerTextures[i], null, (int)hudPos.X + 100 + (i * 32), (int)hudPos.Y + 50, Color.Gray);
        }

        // return false if no buttons clicked
        public bool HandleInput(InputManager input)
        {
            int mouseX = (int)(input.MousePosition.X);
            int mouseY = (int)(input.MousePosition.Y);

            // update to see if mouse is over
            upgrade.Update(mouseX, mouseY);
            sell.Update(mouseX, mouseY);

            for (int i = 0; i < towerButtonList.Length; i++)
                towerButtonList[i].Update(mouseX, mouseY);
            
            // handle clicks
            if (input.LeftClick())
            {
                // click on tower button
                for (int i = 0; i < towerButtonList.Length; i++)
                {
                    if (towerButtonList[i].Hovering)
                    {
                        gameManager.SelectTower((TowerType)i);
                        return true;
                    }
                }

                // click on sell
                if (sell.Hovering)
                {
                    gameManager.SellTower();
                    return true;
                }
                // click on upgrade
                else if (upgrade.Hovering)
                {
                    gameManager.UpgradeTower();
                    return true;
                }
            }
            return false;
        }

        public TowerType TowerHover(int mouseX, int mouseY)
        {
            // cycle through each tower button and see if its been clicked
            for (int i = 0; i < towerButtonList.Length; i++)
            {
                if (towerButtonList[i].Hovering)
                {
                    return (TowerType)i;
                }
            }
            return TowerType.none;
        }

        public bool UpgradeHover(int mouseX, int mouseY)
        {
            if (upgrade.Hovering)
            {
                return true;
            }

            return false;
        }

        public bool SellHover(int mouseX, int mouseY)
        {
            if (sell.Hovering)
            {
                return true;
            }

            return false;
        }

        public void Draw(SpriteBatch spriteBatch)
        {
            // draw hud background
            spriteBatch.Draw(background, hudPos, Color.White);

            // draw buttons
            upgrade.Draw(spriteBatch);
            sell.Draw(spriteBatch);

            for(int i = 0; i < towerButtonList.Length; i++)
            {
                towerButtonList[i].Draw(spriteBatch);
            }

            // draw game info
            spriteBatch.DrawString(arial, "Lives: " + gameManager.Player.Lives, new Vector2(hudPos.X + 350, hudPos.Y + 45), Color.Black);
            //spriteBatch.DrawString(arial, "Wave: " + gameManager.WaveManager.WaveNumber, new Vector2(hudPos.X + 350, hudPos.Y + 75), Color.Black);

            spriteBatch.DrawString(arial, "Money: " + gameManager.Player.Money, new Vector2(hudPos.X + 500, hudPos.Y + 45), Color.Black);
            spriteBatch.DrawString(arial, "Pause - P", new Vector2(hudPos.X + 500, hudPos.Y + 75), Color.Black);
        }

        public void DrawCursor(SpriteBatch spriteBatch, int mouseX, int mouseY, TowerType selectedTower)
        {
            switch (selectedTower)
            {
                case TowerType.fast:
                    spriteBatch.Draw(towerButtonList[(int)TowerType.fast].Texture, new Vector2(mouseX, mouseY), Color.White);
                    break;

                case TowerType.slow:
                    spriteBatch.Draw(towerButtonList[(int)TowerType.slow].Texture, new Vector2(mouseX, mouseY), Color.White);
                    break;

                case TowerType.splash:
                    spriteBatch.Draw(towerButtonList[(int)TowerType.splash].Texture, new Vector2(mouseX, mouseY), Color.White);
                    break;

                case TowerType.poison:
                    spriteBatch.Draw(towerButtonList[(int)TowerType.poison].Texture, new Vector2(mouseX, mouseY), Color.White);
                    break;

                default:
                    break;
            }
        }
    }
}
