﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;
using Microsoft.Xna.Framework.Content;
using Microsoft.Xna.Framework.Input;

//  fast = 0, slow = 1, splash = 2, poison = 3, none 

namespace MyTowerDefence
{
    class GameManager : GameScreen
    {
        private const int MAX_LEVELS = 5;
        private const int MAX_WAVES = 5;

        private Camera camera;
        private Player player;
        private Level level;
        private WaveManager waveManager;
        private HUD hud;

        private Model speedTowerModel;
        private Model slowTowerModel;
        private Model splashTowerModel;
        private Model poisonTowerModel;

        private int currentLevel;
        private TowerType newTower;
        private Tower selectedTower;
        
        // properties
        public Player Player
        {
            get { return player; }
        }
        public WaveManager WaveManager
        {
            get { return waveManager; }
        }
        
        public GameManager(ScreenManager screenManager, int currentLevel)
            : base(screenManager)
        {
            this.currentLevel = currentLevel; // for level select

            hud = new HUD(this);
            player = new Player();
            level = new Level(currentLevel);
            waveManager = new WaveManager(screenManager, level, MAX_WAVES);

            // set up camera
            Vector3 pos = new Vector3(Game.MAP_WIDTH / 2, 12, Game.MAP_HEIGHT / 2 + 8);
            Vector3 at = new Vector3(Game.MAP_WIDTH / 2, 0, Game.MAP_HEIGHT / 2);
            camera = new Camera(pos, at, new Vector2(Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT));

            newTower = TowerType.none;
            selectedTower = null;
        }

        public override void LoadContent(ContentManager content)
        {
            speedTowerModel = content.Load<Model>(@"Models/Towers/towerSpeed");
            slowTowerModel = content.Load<Model>(@"Models/Towers/towerSlow");
            splashTowerModel = content.Load<Model>(@"Models/Towers/towerSplash");
            poisonTowerModel = content.Load<Model>(@"Models/Towers/towerPoison");

            level.LoadContent(content);
            waveManager.LoadContent(content);
            hud.LoadContent(content);
        }

        public override void Update(GameTime gameTime)
        {
            camera.Update();

            // update our towers and handle waves
            foreach (Tower t in player.TowerList)
            {
                t.FindClosestEnemy(waveManager.Enemies);
                t.Update(gameTime);
            }
            /*
            // cycle through enemies to handle splash towers effect
            foreach (Enemy t in waveManager.Enemies)
            {
                if (t.Splashed)
                {
                    t.HandleSplash(waveManager.Enemies);
                }
            }
            */
            waveManager.Update(gameTime, player);
            
            // check if games is over
            if (player.Lives <= 0)
            {
                screenManager.RemoveScreen(this);
                screenManager.AddScreen(new GameOverScreen(screenManager, currentLevel));
            }
            
            // check if next level
            if (waveManager.NoMoreWaves && player.Lives > 0)
            {
                screenManager.RemoveScreen(this);

                if (currentLevel == MAX_LEVELS)
                    screenManager.AddScreen(new GameCompletesScreen(screenManager));

                else if(currentLevel < MAX_LEVELS)
                    screenManager.AddScreen(new LevelCompleteScreen(screenManager, currentLevel));                   
            }
        }

        public override void HandleInput(InputManager input)
        {
            mouseX = (int)input.MousePosition.X;
            mouseY = (int)input.MousePosition.Y;

            #region Shortcuts
            // fast
            if (input.IsKeyPressed(Keys.D1))
                newTower = TowerType.fast;
            //slow
            else if (input.IsKeyPressed(Keys.D2))
                newTower = TowerType.slow;
            //splash
            else if (input.IsKeyPressed(Keys.D3))
                newTower = TowerType.splash;
            //poison
            else if (input.IsKeyPressed(Keys.D4))
                newTower = TowerType.poison;

            // upgrade
            if (input.IsKeyPressed(Keys.U) && selectedTower != null)
                selectedTower.LevelUp();
            // sell shortcut
            if (input.IsKeyPressed(Keys.I) && selectedTower != null)
            {
                player.SellTower(selectedTower);
                selectedTower = null;
            }
            // pause game
            if (input.IsKeyPressed(Keys.P))
            {
                screenManager.AddScreen(new PauseScreen(screenManager, this));
                return;
            }
            //! debugging, remove later
            if (input.IsKeyPressed(Keys.Escape))
                ScreenManager.ExitGame();

            #endregion

            #region Camera
            // move camera
            if (input.IsKeyDown(Keys.W))
                camera.MoveUp();
            if (input.IsKeyDown(Keys.A))
                camera.MoveLeft();
            if (input.IsKeyDown(Keys.S))
                camera.MoveDown();
            if (input.IsKeyDown(Keys.D))
                camera.MoveRight();

            // rotate camera
            if (input.IsKeyDown(Keys.Left))
                camera.RotateYaw(-6.0f);
            if (input.IsKeyDown(Keys.Right))
                camera.RotateYaw(6.0f);
            #endregion

            #region Left click

            // Update hud input and check if a click
            // has beem made in the game world
            if (!hud.HandleInput(input) && input.LeftClick())
            {
                // construct a viewport to make sure the cursor is
                // in the screen space and for rays
                Viewport viewport = new Viewport(0, 0, Game.SCREEN_WIDTH, Game.SCREEN_HEIGHT);
                if (viewport.Bounds.Contains((int)input.MousePosition.X, (int)input.MousePosition.Y))
                {
                    Ray ray = camera.GetMouseRay(input.MousePosition, viewport);
                    GameBlock block = null;
                    Tower tower = null;

                    float z = float.MinValue;
                    int m = -1;


                    // check if tower is clicked
                    for (int i = 0; i < player.TowerList.Count; i++)
                    {
                        // create temp current block
                        tower = player.TowerList[i];

                        // check if intersecting object is closest
                        if (ray.Intersects(tower.BoundingBox) > 0)
                        {
                            if (tower.Position.Z > z)
                            {
                                z = tower.Position.Z;
                                m = i;
                            }
                        }
                    }

                    // placed tower has not been selected
                    if (m == -1)
                    {
                        tower = null;
                        // check if game block is clicked
                        for (int i = 0; i < level.GameBlocks.Count; i++)
                        {
                            // create temp current block
                            block = level.GameBlocks[i];

                            // check if intersecting object is closest
                            if (ray.Intersects(block.BoundingBox) > 0)
                            {
                                if (block.Position.Z > z)
                                {
                                    z = block.Position.Z;
                                    m = i;
                                }
                            }
                        }
                    }
                    
                    // Object clicked
                    if (m >= 0)
                    {
                        // select tower
                        if (tower != null)
                        {
                            selectedTower = tower;
                        }
                        // select block
                        else if (block != null)
                        {
                            Vector3 pos = new Vector3(level.GameBlocks[m].Position.X, 0.5f, level.GameBlocks[m].Position.Z);
                            PlaceTower(pos, level.GameBlocks[m], newTower);
                            newTower = TowerType.none;
                        }
                    }
                }
            }
            #endregion
        }


        public override void Draw(SpriteBatch spriteBatch)
        {
            // selected tower red outline
            if (selectedTower != null)
            {
                //spriteBatch.Draw(towerOutline, selectedTower.Center, null, Color.White, selectedTower.Rotation, selectedTower.Origin, 1.0f, SpriteEffects.None, 0f);
            }

            hud.DrawCursor(spriteBatch,
                            (int)(mouseX - (mouseX % Game.TILE_SIZE)),
                            (int)(mouseY - (mouseY % Game.TILE_SIZE)),
                            newTower);

            hud.Draw(spriteBatch);
        }

        public override void DrawModels()
        {
            level.Draw(camera);

            foreach (Tower t in player.TowerList)
                t.Draw(camera);

            waveManager.Draw(camera);

            base.DrawModels();
        }


        public void SellTower()
        {
            if (selectedTower != null)
            {
                player.SellTower(selectedTower);
                selectedTower = null;
            }
        }

        public void UpgradeTower()
        {
            if (selectedTower != null)
            {
                selectedTower.LevelUp();
                return;
            }
        }

        public void SelectTower(TowerType towerType)
        {
            newTower = towerType;
        }

        public void PlaceTower(Vector3 position, GameBlock block, TowerType towerType)
        {
            if (towerType == TowerType.none)
                return;

            if (block.CanBuildOn == false || block.ContainsTower == true)
                return;

            Tower tower = null;

            switch (towerType)
            {
                case TowerType.fast:
                    tower = new FastTower(position, Vector3.Zero, 1.0f, speedTowerModel, speedTowerModel);
                    break;

                case TowerType.slow:
                    tower = new FastTower(position, Vector3.Zero, 1.0f, slowTowerModel, speedTowerModel);
                    break;

                case TowerType.splash:
                    tower = new FastTower(position, Vector3.Zero, 1.0f, splashTowerModel, speedTowerModel);
                    break;

                case TowerType.poison:
                    tower = new FastTower(position, Vector3.Zero, 1.0f, poisonTowerModel, speedTowerModel);
                    break;

                default:

                    break;
            }

            if (player.Money >= tower.Cost)
            {
                player.TowerList.Add(tower);
                player.Money -= tower.Cost;
            }
        }
    }
}
