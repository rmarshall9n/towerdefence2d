﻿using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Input;
using System.Collections.Generic;

// !! DONT forget to add InputManager.Update(); in the games update method
// !! change the namespace
namespace MyTowerDefence
{
    public class InputManager
    {
        private KeyboardState currentKeyboardState;
        private KeyboardState previousKeyboardState;

        private MouseState currentMouseState;
        private MouseState previousMouseState;
        
        

        public MouseState MouseState
        {
            get { return currentMouseState; }
        }

        public InputManager()
        {
            currentKeyboardState = new KeyboardState();
            previousKeyboardState = new KeyboardState();

            currentMouseState = new MouseState();
            previousMouseState = new MouseState();
        }

        public void Update()
        {
            previousKeyboardState = currentKeyboardState;
            previousMouseState = currentMouseState;

            currentKeyboardState = Keyboard.GetState();
            currentMouseState = Mouse.GetState();
        }

    #region Keyboard Functions
        
        // return true if key is being held
        public bool IsKeyDown(Keys key)
        {
            if(currentKeyboardState.IsKeyDown(key))
                return true;
            else
                return false;
        }

        // return true if key is not being held
        public bool IsKeyUp(Keys key)
        {
            if(currentKeyboardState.IsKeyUp(key))
                return true;
            else
                return false;
        }

        // return true if key has just been pressed
        public bool IsKeyPressed(Keys key)
        {
            if (currentKeyboardState.IsKeyDown(key) && previousKeyboardState.IsKeyUp(key))
                return true;
            else
                return false;
        }

        // return true if key has just been released
        public bool IsKeyReleased(Keys key)
        {
            if (currentKeyboardState.IsKeyUp(key) && previousKeyboardState.IsKeyDown(key))
                return true;
            else
                return false;
        }

        public string ReadString(string theString, int maxLength)
        {
            foreach (Keys key in currentKeyboardState.GetPressedKeys())
            {
                if (previousKeyboardState.IsKeyUp(key))
                {
                    if (theString.Length < maxLength)
                    {
                        if (key >= Keys.A && key <= Keys.Z)
                        {
                            theString += key.ToString();
                        }
                    }
                    if (key == Keys.Back && theString.Length > 0)
                    {
                        theString = theString.Remove(theString.Length - 1, 1);
                    }
                }
            }

            return theString;
        }

#endregion

    #region Mouse Functions

        // checks if the Left button is being held
        public bool LeftButtonDown()
        {
            if (currentMouseState.LeftButton == ButtonState.Pressed)
                return true;

            return false;
        }

        // checks if the Left button was clicked
        public bool LeftClick()
        {
            if (currentMouseState.LeftButton == ButtonState.Released &&
                previousMouseState.LeftButton == ButtonState.Pressed)
                return true;

            return false;
        }

        // returns the positon of the mouse
        public Vector2 MousePosition
        {
            get {
            
                Vector2 pos = new Vector2(currentMouseState.X, currentMouseState.Y);
                return pos; 
            }
        }

        #endregion
    }
}
