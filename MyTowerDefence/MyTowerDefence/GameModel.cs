﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Microsoft.Xna.Framework;
using Microsoft.Xna.Framework.Graphics;

namespace MyTowerDefence
{
    public class GameModel
    {
        private Model model;
        protected Vector3 position;
        protected Vector3 rotation;
        protected float scale;
        protected BoundingBox boundingBox;

        private Matrix worldXform;

        public Model Model
        {
            get { return model; }
        }
        public Vector3 Position
        {
            get { return position; }
            set { position = value; }
        }
        public Vector3 Rotation
        {
            get { return rotation; }
            set { rotation = value; }
        }
        public float Scale
        {
            get { return scale; }
            set { scale = value; }
        }
        public BoundingBox BoundingBox
        {
            get { return boundingBox; }
        }

        public GameModel(Vector3 position, Vector3 rotation, float scale, Model model)
        {
            this.position = position;
            this.rotation = rotation;
            this.scale = scale;
            this.model = model;

            boundingBox.Min = Vector3.Zero;
            boundingBox.Max = Vector3.Zero;

            worldXform = Matrix.Identity;
            worldXform *= Matrix.CreateTranslation(position);
        }

        public virtual void Update(GameTime gameTime)
        {
            worldXform = Matrix.Identity;
            worldXform *= Matrix.CreateScale(scale);
            worldXform *= Matrix.CreateFromYawPitchRoll(rotation.Y, rotation.X, rotation.Z);
            worldXform *= Matrix.CreateTranslation(position);
            
        }

        public virtual void Draw(Camera camera)
        {
            Matrix[] transforms = new Matrix[model.Bones.Count];
            model.CopyAbsoluteBoneTransformsTo(transforms);

            foreach (ModelMesh mesh in model.Meshes)
            {
                foreach (BasicEffect effect in mesh.Effects)
                {
                    effect.EnableDefaultLighting();
                    effect.View = camera.ViewXform;
                    effect.Projection = camera.ProjXform;
                    effect.World = transforms[mesh.ParentBone.Index] * worldXform;
                }
                mesh.Draw();
            }
        }
    }
}
